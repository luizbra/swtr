const express = require('express')
const cors = require('cors')
const webtorrent = require('webtorrent')

const app = express()
const client = new webtorrent()
const streams = []

app.use(express.json())
app.use(cors())

app.get('/', (req, res) => {
    res.json({
        numTorrents: client.torrents.length,
        downloadSpeed: prettyBytes(client.downloadSpeed),
        uploadSpeed: prettyBytes(client.uploadSpeed),
    })
})

app.get('/torrents', (req, res) => {
    const torrents = []

    client.torrents.forEach((torrent, i) => {
        const files = []

        torrent.files.forEach((file, i) => {
            files.push({
                id: i,
                name: file.name,
                progress: `${(file.progress * 100).toFixed()}%`,
            })
        })
        torrents.push({
            id: i,
            name: torrent.name,
            size: prettyBytes(torrent.length),
            progress: `${(torrent.progress * 100).toFixed()}%`,
            numPeers: torrent.numPeers,
            downloadSpeed: `${prettyBytes(torrent.downloadSpeed)}/s`,
            uploadSpeed: `${prettyBytes(torrent.uploadSpeed)}/s`,
            ratio: torrent.ratio.toFixed(2),
            files: files
        })
    })
    res.json({torrents: torrents})
})

app.post('/torrents', (req, res) => {
    if (req.body.magnetUri && req.body.magnetUri.match(/^magnet:\?xt=urn:[a-z0-9]+:[a-z0-9]{32}/i)) {
        if (!client.get(req.body.magnetUri)) {
            client.add(req.body.magnetUri, { destroyStoreOnDestroy: true },
                torrent => {
                    torrent.on('noPeers', () => {
                        torrent.destroy()
                    })
            })
            res.status(201).json({ status: 'success', message: 'Torrent added.' })
        } else {
            res.status(400).json({ status: 'error', message: 'Torrent already exists.' })
        }
    } else {
        res.status(400).json({ status: 'error', message: 'Bad magnet link.' })
    }
})

app.post('/torrents/:torrentId', (req, res) => {
    const torrent = client.torrents[req.params.torrentId]

    if (torrent) {
        if (torrent.files[req.body.fileId]) {
            torrent.files.forEach((file, i) => {
                if (req.body.fileId == i) {
                    file.select()
                } else {
                    file.deselect()
                }
            })
            res.status(201).json({ status: 'success', message: 'File selected.' })
        } else {
            res.status(404).json({ status: 'error', message: 'File not found.' })
        }
    } else {
        res.status(404).json({ status: 'error', message: 'Torrent not found.' })
    }
})

app.delete('/torrents/:torrentId', (req, res) => {
    const torrent = client.torrents[req.params.torrentId]

    if (torrent) {
        streams.forEach((stream, i) => {
            if (req.params.torrentId == stream.torrentId) {
                const server = stream.server
                server.close()
                streams.splice(i, 1)
            }
        })
        torrent.destroy()
        res.json({ status: 'success', message: 'Torrent removed.' })
    } else {
        res.status(404).json({ status: 'error', message: 'Torrent not found.' })
    }
})

app.get('/streams', (req, res) => {
    const streamsJson = []

    streams.forEach((stream, i) => {
        streamsJson.push({
            id: i,
            torrentId: stream.torrentId,
            port: stream.server.address().port
        })
    })
    res.json({ streams: streamsJson })
})

app.post('/streams', (req, res) => {
    const torrent = client.torrents[req.body.torrentId]

    if (torrent) {
        const server = torrent.createServer()
        streamId = streams.push({
            torrentId: req.body.torrentId,
            server: server
        })
        server.on('error', error => {
            server.close()
            streams.splice(streamId -1, 1)
        })
        server.listen()
        res.status(201).json({ status: 'success', message: 'Stream added.' })
    } else {
        res.status(404).json({ status: 'error', message: 'Torrent not found.' })
    }
})

app.delete('/streams/:streamId', (req, res) => {
    if (streams[req.params.streamId]) {
        streams.forEach((stream, i) => {
            if (req.params.streamId == i) {
                stream.server.close()
                streams.splice(i, 1)
            }
        })
        res.json({ status: 'success', message: 'Torrent deleted.' })
    } else {
        res.status(404).json({ status: 'error', message: 'Stream not found.' })
    }
})

function prettyBytes(num) {
    var exponent, unit, neg = num < 0, units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    if (neg) num = -num
    if (num < 1) return (neg ? '-' : '') + num + ' B'
    exponent = Math.min(Math.floor(Math.log(num) / Math.log(1000)), units.length - 1)
    num = Number((num / Math.pow(1000, exponent)).toFixed(2))
    unit = units[exponent]
    return (neg ? '-' : '') + num + ' ' + unit
}

module.exports = { app: app, client: client }
