#!/usr/bin/env node

const host = '127.0.0.1'
const port = 8080

const app = require('./app').app

app.listen(port, host, () => {
    console.log(`Server running at http://${host}:${port}/`)
})
