const host = '127.0.0.1'
const port = 8080

refreshTorrents()

async function refreshTorrents() {
    const response = await fetch(`http://${host}:${port}/torrents`)
        .then(r => r.json())
    const streamsResponse = await fetch(`http://${host}:${port}/streams`)
        .then(r => r.json())
    const tbody = document.querySelector('tbody')

    while(tbody.firstChild) {
        tbody.removeChild(tbody.firstChild)
    }

    updateButtons()

    response.torrents.forEach(torrent => {
        const tr = document.createElement('tr')
        const th = document.createElement('th')
        const checkbox = document.createElement('input')
        const tdName = document.createElement('td')
        const tdSize = document.createElement('td')
        const tdDone = document.createElement('td')
        const tdPeers = document.createElement('td')
        const tdDown = document.createElement('td')
        const tdUp = document.createElement('td')
        const tdRatio = document.createElement('td')

        th.setAttribute('scope', 'row')
        checkbox.setAttribute('id', torrent.id)
        checkbox.setAttribute('type', 'checkbox')
        checkbox.setAttribute('onclick', 'updateButtons()')
        checkbox.classList.add('form-check-input')

        tdSize.appendChild(document.createTextNode(torrent.size))
        tdDone.appendChild(document.createTextNode(torrent.progress))
        tdPeers.appendChild(document.createTextNode(torrent.numPeers))
        tdDown.appendChild(document.createTextNode(torrent.downloadSpeed))
        tdUp.appendChild(document.createTextNode(torrent.uploadSpeed))
        tdRatio.appendChild(document.createTextNode(torrent.ratio))

        th.append(checkbox)
        tr.append(th)

        let streamPort = 0
        streamsResponse.streams.forEach(stream => {
            if (stream.torrentId == torrent.id) streamPort = stream.port
        })
        if (streamPort) {
            const link = document.createElement('a')

            link.classList.add('text-decoration-none')
            link.setAttribute('href', `http://${host}:${streamPort}/`)
            link.setAttribute('target', '_blank')
            link.appendChild(document.createTextNode(torrent.name))
            tdName.append(link)
        } else {
            tdName.appendChild(document.createTextNode(torrent.name))
        }

        tr.append(tdName)
        tr.append(tdSize)
        tr.append(tdDone)
        tr.append(tdPeers)
        tr.append(tdDown)
        tr.append(tdUp)
        tr.append(tdRatio)
        tbody.append(tr)
    })
}

async function addTorrent() {
    const inputMagnet = document.querySelector('#magnet')
    const response = await fetch(`http://${host}:${port}/torrents`, {
        method: 'POST',
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify({ magnetUri: inputMagnet.value })
    }).then(r => r.json())

    addToast(
        (response.status == 'success') ? response.status : 'danger',
        response.status.replace(/^\w/, c => c.toUpperCase()),
        response.message
    )

    inputMagnet.value = ''
    document.querySelector('#addModal .btn-close').click()
    await sleep(3)
    refreshTorrents()
}

async function removeTorrents() {
    const torrents = document.querySelectorAll('tbody input:checked')

    torrents.forEach(async torrent => {
        const response = await fetch(
            `http://${host}:${port}/torrents/${torrent.id}`, { method: 'DELETE' }
        ).then(r => r.json())

        addToast(
            (response.status == 'success') ? response.status : 'danger',
            response.status.replace(/^\w/, c => c.toUpperCase()),
            response.message
        )
    })

    document.querySelector('#removeModal .btn-close').click()
    await sleep(1)
    refreshTorrents()
}

async function selectFiles(flag) {
    if (flag) {
        const modal = document.querySelector('#filesModal .modal-body')
        const torrents = document.querySelectorAll('tbody input:checked')
        const response = await fetch(`http://${host}:${port}/torrents`)
            .then(r => r.json())

        while (modal.firstChild) {
            modal.removeChild(modal.firstChild)
        }

        torrents.forEach(torrent => {
            const label = document.createElement('label')
            const select = document.createElement('select')

            label.classList.add('form-label')
            label.setAttribute('for', `select-${torrent.id}`)
            label.appendChild(document.createTextNode(response.torrents[torrent.id].name))
            select.classList.add('form-select')
            select.setAttribute('id', `select-${torrent.id}`)
            response.torrents[torrent.id].files.forEach((file, i) => {
                const option = document.createElement('option')

                option.setAttribute('value', i)
                option.appendChild(document.createTextNode(file.name))
                select.append(option)
            })
            modal.append(label)
            modal.append(select)
        })

        new bootstrap.Modal(document.querySelector('#filesModal')).show()
    } else {
        const selects = document.querySelectorAll('#filesModal select')

        selects.forEach(async select => {
            const response = await fetch(
                `http://${host}:${port}/torrents/${select.id.split('-')[1]}`,
                {
                    method: 'POST',
                    headers: { 'Content-type': 'application/json' },
                    body: JSON.stringify({ fileId: select.value })
                }
            ).then(r => r.json())

            addToast(
                (response.status == 'success') ? response.status : 'danger',
                response.status.replace(/^\w/, c => c.toUpperCase()),
                response.message
            )

            document.querySelector('#filesModal .btn-close').click()
            await sleep(1)
            refreshTorrents()
        })
    }
}

async function streamTorrents() {
    const torrents = document.querySelectorAll('tbody input:checked')

    torrents.forEach(async torrent => {
        const response = await fetch(`http://${host}:${port}/streams`, {
            method: 'POST',
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({ torrentId: torrent.id })
        }).then(r => r.json())
        addToast(
            (response.status == 'success') ? response.status : 'danger',
            response.status.replace(/^\w/, c => c.toUpperCase()),
            response.message
        )
        await sleep(3)
        refreshTorrents()
    })
}

function addToast(color, title, message) {
    const container = document.querySelector('.toast-container')
    const toast = document.createElement('div')
    const toastHeader = document.createElement('div')
    const toastTitle = document.createElement('strong')
    const toastDismiss = document.createElement('button')
    const toastBody = document.createElement('div')

    toast.classList.add('toast')
    toastHeader.classList.add('toast-header')
    toastTitle.classList.add('me-auto')
    toastTitle.classList.add(`text-${color}`)
    toastDismiss.classList.add('btn-close')
    toastDismiss.setAttribute('data-bs-dismiss', 'toast')
    toastBody.classList.add('toast-body')

    toastTitle.appendChild(document.createTextNode(title))
    toastBody.appendChild(document.createTextNode(message))

    toastHeader.append(toastTitle)
    toastHeader.append(toastDismiss)
    toast.append(toastHeader)
    toast.append(toastBody)
    container.append(toast)

    new bootstrap.Toast(toast).show()
}

function updateButtons() {
    const checked = document.querySelectorAll('tbody input:checked')

    if (checked.length > 0) {
        document.querySelectorAll('.button-toggle').forEach(button => {
            button.removeAttribute('disabled')
        })
    } else {
        document.querySelector('thead input[type=checkbox]').checked = false
        document.querySelectorAll('.button-toggle').forEach(button => {
            button.setAttribute('disabled', true)
        })
    }
}

function toggleAll(elem) {
    document.querySelectorAll('tbody input[type=checkbox]').forEach(checkbox => {
        checkbox.checked = elem.checked
    })
    updateButtons()
}

function sleep(seconds) {
    return new Promise(resolve => setTimeout(resolve, seconds * 1000))
}
