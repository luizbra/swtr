jest.setTimeout(15000)

const app = require('./app').app
const client = require('./app').client
const request = require('supertest')

const torrent = {
    magnetUri: 'magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent',
    name: 'Sintel',
    numFiles: 11
}

describe('Adding torrents', () => {
    it('Threats a bad request.', async () => {
        const res = await request(app).post('/torrents').send({})

        expect(res.statusCode).toBe(400)
        expect(res.body.status).toBe('error')
    })

    it('Adds a torrent.', async () => {
        const res = await request(app).post('/torrents').send({
            magnetUri: torrent.magnetUri
        })

        expect(res.statusCode).toBe(201)
        expect(res.body.status).toBe('success')
        expect(client.torrents.length).toBe(1)
    })

    it('Does not allow adding the same torrent.', async () => {
        const res = await request(app).post('/torrents').send({
            magnetUri: torrent.magnetUri
        })

        expect(res.statusCode).toBe(400)
        expect(res.body.status).toBe('error')
        expect(client.torrents.length).toBe(1)
    })
})

describe("Selecting torrent's files", () => {
    it('Selects a file from torrent.', async () => {
        await sleep(3)
        const res = await request(app).post('/torrents/0').send({ fileId: torrent.numFiles - 1 })

        expect(res.statusCode).toBe(201)
        expect(res.body.status).toBe('success')
    })

    it('Returns 404 on nonexistent file.', async () => {
        const res = await request(app).post('/torrents/0').send({})

        expect(res.statusCode).toBe(404)
        expect(res.body.status).toBe('error')
    })

})

describe('Adding streams', () => {
    it ('Returns 404 on nonexistent torrent.', async () => {
        const res = await request(app).post('/streams').send({})

        expect(res.statusCode).toBe(404)
        expect(res.body.status).toBe('error')
    })

    it('Adds a stream.', async () => {
        const res = await request(app).post('/streams').send({ torrentId: 0 })

        expect(res.statusCode).toBe(201)
        expect(res.body.status).toBe('success')
    })

    it('Allows multiple streams to same torrent.', async () => {
        const res = await request(app).post('/streams').send({ torrentId: 0 })

        expect(res.statusCode).toBe(201)
        expect(res.body.status).toBe('success')
    })
})

describe('Accessing streams', () => {
    it('Shows running streams.', async () => {
        const res = await request(app).get('/streams')

        expect(res.statusCode).toBe(200)
        expect(res.body.streams.length).toBe(2)
        expect(res.body.streams[0].torrentId).toBe(0)
    })

    it('Returns 404 on nonexistent stream.', async () => {
        const res = await request(app).delete('/streams/2')

        expect(res.statusCode).toBe(404)
        expect(res.body.status).toBe('error')
    })

    it('Removes a stream.', async () => {
        const res = await request(app).delete('/streams/0')

        expect(res.statusCode).toBe(200)
        expect(res.body.status).toBe('success')

        {
            const res = await request(app).get('/streams')
            
            expect(res.statusCode).toBe(200)
            expect(res.body.streams.length).toBe(1)
        }
    })
})

describe('Removing torrents', () => {
    it('Removes a torrent.', async () => {
        const res = await request(app).delete('/torrents/0')

        expect(res.statusCode).toBe(200)
        expect(res.body.status).toBe('success')
        expect(client.torrents.length).toBe(0)
    })

    it("Also removes all of torrent's streams.", async () => {
        const res = await request(app).get('/streams')

        expect(res.statusCode).toBe(200)
        expect(res.body.streams.length).toBe(0)
    })

    it('Returns 404 on nonexistent torrent.', async () => {
        const res = await request(app).delete('/torrents/1')

        expect(res.statusCode).toBe(404)
        expect(res.body.status).toBe('error')
    })
})

afterAll(() => client.destroy())

function sleep(seconds) {
  return new Promise(resolve => setTimeout(resolve, seconds * 1000))
}
